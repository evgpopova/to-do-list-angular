# TO DO List Angular Project

This is a very simple Angular application - To Do List. And it is based on
[tutorial](https://angular.io/tutorial)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites


```
npm
```

```
node.js
```

### Installing

Install npm dependencies:
```
npm install
```

Start the project:
```
npm start
```
