"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var item_service_1 = require("./item.service");
var AddingComponent = (function () {
    function AddingComponent(itemService, router) {
        this.itemService = itemService;
        this.router = router;
        this.items = [];
    }
    AddingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.itemService.getItems()
            .then(function (items) { return _this.items = items.slice(0, 2); });
    };
    AddingComponent.prototype.add = function (action, start, end) {
        var _this = this;
        action = action.trim();
        if (!action) {
            return;
        }
        this.itemService.create(action, start, end)
            .then(function (item) {
            _this.items.push(item);
        });
        this.router.navigate(['/items']);
    };
    return AddingComponent;
}());
AddingComponent = __decorate([
    core_1.Component({
        selector: 'adding-item',
        templateUrl: './adding.component.html',
        styleUrls: ['./adding.component.css']
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService,
        router_1.Router])
], AddingComponent);
exports.AddingComponent = AddingComponent;
//# sourceMappingURL=adding.component.js.map