"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ITEMS = [
    { id: 1, action: 'to sleep', start: '22:10', end: '07:00' },
    { id: 2, action: 'to eat', start: '08:00', end: '08:30' },
    { id: 3, action: 'to learn JS', start: '18:30', end: '21:00' }
];
//# sourceMappingURL=mock-items.js.map