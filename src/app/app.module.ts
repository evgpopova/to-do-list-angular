import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule}     from '@angular/http';

import { AppComponent }  from './app.component';
import { ItemDetailComponent} from './item-detail.component';
import { ItemsComponent} from './items.component';
import { ItemService} from './item.service';
import { AddingComponent } from './adding.component';

import { AppRoutingModule } from './app-routing.module';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService)
    ],
    declarations: [
        AppComponent,
        ItemDetailComponent,
        ItemsComponent,
        AddingComponent,
    ],
    providers: [ ItemService ],
    bootstrap: [ AppComponent ]
})

export class AppModule { }
