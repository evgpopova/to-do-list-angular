import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from './item';
import { ItemService } from './item.service';

@Component({
    selector: 'adding-item',
    templateUrl: './adding.component.html',
    styleUrls: ['./adding.component.css']
})

export class AddingComponent implements OnInit {
    items: Item[] = [];
    constructor (
        private itemService: ItemService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.itemService.getItems()
            .then(items => this.items = items.slice(0, 2));
    }
    add(action: string, start: string, end: string): void {
        action = action.trim();
        if (!action) { return; }
        this.itemService.create(action, start, end)
            .then(item => {
                this.items.push(item);
            });
        this.router.navigate(['/items']);
    }
}
