"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InMemoryDataService = (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        var items = [
            { id: 1, action: 'to sleep', start: '22:10', end: '07:00' },
            { id: 2, action: 'to eat', start: '08:00', end: '08:30' },
            { id: 3, action: 'to learn JS', start: '19:50', end: '21:00' },
            { id: 4, action: 'to work', start: '10:00', end: '19:00' },
            { id: 5, action: 'to get a dinner', start: '19:10', end: '19:50' },
            { id: 7, action: 'to live', start: '00:00', end: '23:59' }
        ];
        return { items: items };
    };
    return InMemoryDataService;
}());
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map