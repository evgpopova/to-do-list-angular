import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ItemDetailComponent} from './item-detail.component';
import { ItemsComponent} from './items.component';
import { AddingComponent } from './adding.component';

const routes: Routes = [
    { path: '', redirectTo: '/items', pathMatch: 'full'},
    { path: 'items', component: ItemsComponent},
    { path: 'add', component: AddingComponent},
    { path: 'detail/:id', component: ItemDetailComponent}
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
