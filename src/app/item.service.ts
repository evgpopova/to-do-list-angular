import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Item } from './item';


@Injectable()
export class ItemService {
    private itemsUrl = 'api/items';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) { }

    getItems(): Promise<Item[]> {
        return this.http.get(this.itemsUrl)
            .toPromise()
            .then(response => response.json().data as Item[])
            .catch(this.handleError);
    };
    handleError(error: any): Promise<any> {
        console.error('An error occurred ', error);
        return Promise.reject(error.message || error);
    }
    getItem(id: number): Promise<Item> {
        const url = `${this.itemsUrl}/${id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().data as Item)
            .catch(this.handleError);
    }
    update(item: Item): Promise<Item> {
        const url = `${this.itemsUrl}/${item.id}`;
        return this.http.put(url, JSON.stringify(item), {headers: this.headers})
            .toPromise()
            .then(() => item)
            .catch(this.handleError);
    }
    create(action: string, start: string, end: string): Promise<Item> {
        return this.http
            .post(this.itemsUrl, JSON.stringify({action: action, start: start, end: end}), {headers: this.headers})
            .toPromise()
            .then(res => res.json() as Item)
            .catch(this.handleError);
    }
    /*
    getItemsSlowly() {
        return new Promise(resolve => {
          // Simulate server latency with 2 second delay
          setTimeout(() => resolve(this.getItems()), 2000);
        });
    }*/
}

